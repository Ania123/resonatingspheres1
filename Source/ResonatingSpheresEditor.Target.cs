// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class ResonatingSpheresEditorTarget : TargetRules
{
	public ResonatingSpheresEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

        // enables errors in include what you use
        bUseUnityBuild = false;
        bUsePCHFiles = false;
        //

		ExtraModuleNames.AddRange( new string[] { "ResonatingSpheres" } );
	}
}
