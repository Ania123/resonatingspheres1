// Ani a

#include "MyPlayerController.h"
#include "BuddyController.h"
#include "BuddyPawn.h"
#include "GameManager.h"

void AMyPlayerController::BeginPlay() 
{
	InputComponent->BindAction("SwitchPawn", EInputEvent::IE_Pressed, this, &AMyPlayerController::SwitchBuddy);
	InputComponent->BindAction("EmitSound", EInputEvent::IE_Pressed, this, &AMyPlayerController::EmitSound);
}

void AMyPlayerController::EmitSound()
{
	auto gm = FindComponentByClass<UGameManager>();
	auto buddy = gm->buddies[buddyCounter];
	if (buddy != nullptr) {
		buddy->EmitSound();
	}
}

void AMyPlayerController::SwitchBuddy() {
	UE_LOG(LogTemp, Warning, TEXT("Switching pawn"));
	bAutoManageActiveCameraTarget = false;
	auto gm = FindComponentByClass<UGameManager>();
	buddyCounter++;
	buddyCounter = buddyCounter % gm->buddies.Num();
	auto newBuddyToPosess = gm->buddies[buddyCounter];
	auto buddyPawn =  Cast<ABuddyPawn>(newBuddyToPosess->GetOwner());
	buddyPawn->cameraOverride = Cast<ABuddyPawn>(GetPawn())->cameraOverride;
	if (buddyPawn->cameraOverride == nullptr) {
		SetViewTargetWithBlend(buddyPawn, 2.0f, EViewTargetBlendFunction::VTBlend_Cubic, 2.0f, false);
	}
	Possess(buddyPawn);
}
