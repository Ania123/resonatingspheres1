// Ani a

#include "FollowObjectPositionInMetaballs.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
UFollowObjectPositionInMetaballs::UFollowObjectPositionInMetaballs()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UFollowObjectPositionInMetaballs::BeginPlay()
{
	Super::BeginPlay();

	// ...
	gameObject = GetOwner();
	gameManager = GetWorld()->GetFirstPlayerController()->FindComponentByClass<UGameManager>();
	metaballs = gameManager->metaballs;
	cycleOffset = UKismetMathLibrary::MultiplyByPi(float(rand()) / RAND_MAX);
}


// Called every frame
void UFollowObjectPositionInMetaballs::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FVector loc = gameObject->GetTransform().GetLocation();
	
	loc += gameObject->GetActorForwardVector()*offset.GetLocation().X;
	loc += gameObject->GetActorUpVector()*offset.GetLocation().Z;
	
	if (metaballs == nullptr) {
		gameManager = GetWorld()->GetFirstPlayerController()->FindComponentByClass<UGameManager>();
		metaballs = gameManager->metaballs;
	}

	if (metaballs != nullptr) 
	{
		metaballs->SetBallTransformFromRealWorld(metaballIndex, loc);
		metaballs->SetBallRadiusFromRealWorld(metaballIndex, radius);
	}
	else 
	{
		//UE_LOG(LogTemp, Warning, TEXT("no metaballs, so sad :("));
	}
}

