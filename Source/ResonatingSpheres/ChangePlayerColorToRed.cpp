// Ani a

#include "ChangePlayerColorToRed.h"


// Sets default values for this component's properties
UChangePlayerColorToRed::UChangePlayerColorToRed()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	
	// ...
}


// Called when the game starts
void UChangePlayerColorToRed::BeginPlay()
{
	Super::BeginPlay();

	gameObject = GetOwner();
	myMesh = gameObject->FindComponentByClass<UStaticMeshComponent>();
	UMaterialInterface * existingMat = myMesh->GetMaterial(0);

	dynamicMat = UMaterialInstanceDynamic::Create(existingMat, this);
	dynamicMat->SetScalarParameterValue(FName("JellyColorOpacity"), FMath::Sin(GetWorld()->GetTimeSeconds()));
	myMesh->SetMaterial(0, dynamicMat);
	// ...
	
}


// Called every frame
void UChangePlayerColorToRed::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	dynamicMat->SetScalarParameterValue(FName("JellyColorOpacity"), FMath::Sin(GetWorld()->GetTimeSeconds()));
	// ...
}

