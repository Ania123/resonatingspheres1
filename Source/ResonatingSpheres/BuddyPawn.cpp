// Ani a

#include "BuddyPawn.h"
#include "Classes/Components/InputComponent.h"

void ABuddyPawn::SetupPlayerInputComponent(UInputComponent * PlayerInputComponent)
{
	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &ABuddyPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABuddyPawn::MoveRight);
	PlayerInputComponent->BindAxis("Turn", this, &ADefaultPawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &ADefaultPawn::AddControllerPitchInput);
}

void ABuddyPawn::MoveRight(float Val)
{
	if (Val != 0.f)
	{
		if (Controller)
		{
			FRotator ControlSpaceRot;
			if (cameraOverride != nullptr) {
				ControlSpaceRot = cameraOverride->GetActorRotation();
			}
			else {
				ControlSpaceRot = Controller->GetControlRotation();
			}

			// transform to world space and add it
			AddMovementInput(FRotationMatrix(ControlSpaceRot).GetScaledAxis(EAxis::Y), Val);
		}
	}
}

void ABuddyPawn::MoveForward(float Val)
{
	if (Val != 0.f)
	{
		if (Controller)
		{
			FRotator ControlSpaceRot;
			if (cameraOverride != nullptr) {
				ControlSpaceRot = cameraOverride->GetActorRotation();
			}
			else {
				ControlSpaceRot = Controller->GetControlRotation();
			}


			// transform to world space and add it
			AddMovementInput(FRotationMatrix(ControlSpaceRot).GetScaledAxis(EAxis::X), Val);
		}
	}
}

void ABuddyPawn::OnTweenStartNative(AiTweenEvent * eventOperator, AActor * actorTweening, USceneComponent * componentTweening, UWidget * widgetTweening, FName tweenName)
{
}

void ABuddyPawn::OnTweenUpdateNative(AiTweenEvent * eventOperator, AActor * actorTweening, USceneComponent * componentTweening, UWidget * widgetTweening, FName tweenName, FDataTypeValues dataTypeValues, float alphaCompletion)
{
}

void ABuddyPawn::OnTweenLoopNative(AiTweenEvent * eventOperator, AActor * actorTweening, USceneComponent * componentTweening, UWidget * widgetTweening, FName tweenName, int32 numberOfLoopSections, ELoopType::LoopType loopType, bool playingBackward)
{
	UE_LOG(LogTemp, Warning, TEXT("On tween end from %s"), *GetName());
	OnMovementTweenEnded.Broadcast();
}

void ABuddyPawn::OnTweenCompleteNative(AiTweenEvent * eventOperator, AActor * actorTweening, USceneComponent * componentTweening, UWidget * widgetTweening, FName tweenName, FHitResult sweepHitResultForMoveEvents, bool successfulTransform)
{
	UE_LOG(LogTemp, Warning, TEXT("On tween end from %s"), *GetName());
	OnMovementTweenEnded.Broadcast();
}
