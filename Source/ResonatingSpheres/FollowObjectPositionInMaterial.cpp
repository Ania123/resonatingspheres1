// Ani a

#include "FollowObjectPositionInMaterial.h"
#include "GameManager.h"
#include "ChangePlayerColorToRed.h"

// Sets default values for this component's properties
UFollowObjectPositionInMaterial::UFollowObjectPositionInMaterial()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UFollowObjectPositionInMaterial::BeginPlay()
{
	Super::BeginPlay();
	gameObject = GetOwner();
	// ...
	dynamicMaterialInstanceManager = gameObject->FindComponentByClass<UChangePlayerColorToRed>();
	gameManager = GetWorld()->GetFirstPlayerController()->GetPawn()->FindComponentByClass<UGameManager>();
}


// Called every frame
void UFollowObjectPositionInMaterial::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	FTransform trans = gameObject->GetTransform();
	FVector  loc  = trans.GetLocation();
	FLinearColor col = FLinearColor(loc.X, loc.Y, loc.Z, sphereRadius);
	gameManager->spherePositions->Add(sphereName, col);
	for (const TPair<FName, FLinearColor>& pair : *gameManager->spherePositions)
	{
		dynamicMaterialInstanceManager->dynamicMat->SetVectorParameterValue(pair.Key, pair.Value);
	}
}

