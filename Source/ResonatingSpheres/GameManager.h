// Ani a

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Metaballs.h"
#include "GameManager.generated.h"


class UBuddyController;
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCatastrophyOccured);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RESONATINGSPHERES_API UGameManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGameManager();

	UPROPERTY(EditAnywhere)
	TArray<UBuddyController*> buddies;

	TMap<FName, FLinearColor>* spherePositions;

	UPROPERTY(EditAnywhere)
		float worldBounds = 1000;

	UPROPERTY(VisibleAnywhere)
	AMetaballs * metaballs;


	UPROPERTY(BlueprintAssignable)
		FCatastrophyOccured OnCatastrophyOccured;

	void DoCatastrophe();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	bool wasCatastrophe;
	
};
