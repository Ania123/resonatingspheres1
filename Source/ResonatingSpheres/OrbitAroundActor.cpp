// Ani a

#include "OrbitAroundActor.h"


// Sets default values for this component's properties
UOrbitAroundActor::UOrbitAroundActor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOrbitAroundActor::BeginPlay()
{
	Super::BeginPlay();
	gameObject = GetOwner();
	CurrentOrbitRadius = FVector(InitialOrbitRadius, 0, 0);
	TooCloseTimeElapsed = 0.0f;
	wasCatastrophe = false;
	gameManager = GetWorld()->GetFirstPlayerController()->FindComponentByClass<UGameManager>();
	gameObject->SetActorLocation(((gameObject->GetActorLocation() - PersonToOrbitAround->GetActorLocation())).GetClampedToSize(1.0f, 1.0f)*CurrentOrbitRadius + PersonToOrbitAround->GetActorLocation());
	AngleAxis = 0.0f;
	AxisVector = FVector( 0.0f, 0.0f, 1.0f);
}


// Called every frame
void UOrbitAroundActor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (wasCatastrophe) {
		return;
	}

	AngleAxis += DeltaTime * RotationSpeed;
	AngleAxis = FMath::Fmod(AngleAxis, 360.0);


	if (CurrentOrbitRadius.X >= TooCloseOrbitRadius) {
		CurrentOrbitRadius.X -= DeltaTime * CloserSpeed;
	}
	else {
		TooCloseTimeElapsed += DeltaTime;
		if (TooCloseTimeElapsed > TimeForCatastrophe) {
			wasCatastrophe = true;
			gameManager->DoCatastrophe();
			CurrentOrbitRadius = FVector(1000.0f, 0, 0);
			AngleAxis = 0.0f;
		}
	}

	FVector NewLocation = PersonToOrbitAround->GetActorLocation();

	FVector RotateValue = CurrentOrbitRadius.RotateAngleAxis(AngleAxis, AxisVector);

	NewLocation.X += RotateValue.X;
	NewLocation.Y += RotateValue.Y;
	NewLocation.Z += RotateValue.Z;

	FRotator NewRotation = FRotator(0, AngleAxis, 0);

	FQuat QuatRotation = FQuat(NewRotation);

	gameObject->SetActorLocationAndRotation(NewLocation, QuatRotation, false, 0, ETeleportType::None);
}

