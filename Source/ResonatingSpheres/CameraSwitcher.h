// Ani a

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CameraSwitcher.generated.h"

UCLASS()
class RESONATINGSPHERES_API ACameraSwitcher : public AActor
{
	GENERATED_UCLASS_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACameraSwitcher();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void KeyOnePressed();

	void KeyTwoPressed();

	void KeyThreePressed();

	void KeyFourPressed();

	void KeyFivePressed();

	void KeySixPressed();

	void KeySevenPressed();

	void KeyPressed(int i);

public:	
	UPROPERTY(EditAnywhere)
		float blendTime = 2.0f;
	UPROPERTY(EditAnywhere)
	 TArray<AActor*> cameras;
private:
	UInputComponent * inputComponent = nullptr;
	APlayerController* playerController;
	AActor* playerCharacter;
	AActor* currentCamera;
	void RemovePlayerInput();
	void RegainInputAfterBlend();
	bool blocked;
};
