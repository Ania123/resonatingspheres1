// Ani a

#include "GameManager.h"


// Sets default values for this component's properties
UGameManager::UGameManager()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	spherePositions = new TMap<FName, FLinearColor>();
	// ...
}


// Called when the game starts
void UGameManager::BeginPlay()
{
	Super::BeginPlay();	
}

void UGameManager::DoCatastrophe() {
	if (!wasCatastrophe) {
		UE_LOG(LogTemp, Warning, TEXT("CATASTROPHE"));
		wasCatastrophe = true;
		OnCatastrophyOccured.Broadcast();
	}
}
// Called every frame
void UGameManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

