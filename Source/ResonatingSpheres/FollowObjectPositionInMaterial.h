// Ani a

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "FollowObjectPositionInMaterial.generated.h"

class UGameManager;
class UChangePlayerColorToRed;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RESONATINGSPHERES_API UFollowObjectPositionInMaterial : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFollowObjectPositionInMaterial();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY(EditAnywhere)
		FName sphereName;

	UPROPERTY(EditAnywhere)
		float sphereRadius;
	
	UGameManager * gameManager;

	UStaticMeshComponent * myMesh;

	AActor * gameObject;

	UChangePlayerColorToRed * dynamicMaterialInstanceManager;
};
