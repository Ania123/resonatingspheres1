// Ani a

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerComponent.generated.h"

class UBuddyController;
class UInputComponent;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RESONATINGSPHERES_API UPlayerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerComponent();

	UFUNCTION(BlueprintCallable)
		void NotifyPresence(UBuddyController* objectNotifying);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	UInputComponent * inputComponent = nullptr;
	UBuddyController * buddy = nullptr;
	void EmitSound();
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

};
