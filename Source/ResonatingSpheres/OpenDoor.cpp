// Ani a

#include "OpenDoor.h"
#include "Classes/Components/PrimitiveComponent.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	// ...
	gameObject = GetOwner();
}

void UOpenDoor::OpenDoor()
{
	OnOpenRequest.Broadcast();
	//gameObject->SetActorRotation(FRotator(0, OpenAngle, 0));
}

void UOpenDoor::CloseDoor()
{
	OnCloseRequest.Broadcast();
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	bool open = GetTotalMassOfActorsOnPlate()>50.0f;
	if (open) {
		OpenDoor();
		LastDoorOpenTime = GetWorld()->TimeSeconds;
	}
	else {
		if (GetWorld()->TimeSeconds - LastDoorOpenTime > DoorCloseDelay) {
			CloseDoor();
		}
	}
}

float UOpenDoor::GetTotalMassOfActorsOnPlate() {
	float result = 0.0f;
	TArray<AActor*> overlappingActors;
	PressurePlate->GetOverlappingActors(overlappingActors);
	for (auto actor : overlappingActors) {
		result += actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}
	return result;
}

