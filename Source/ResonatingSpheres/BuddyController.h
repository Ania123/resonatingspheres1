// Ani a

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "iTAux.h"
#include "Void.h"
#include "BuddyController.generated.h"

using namespace EEaseType;


class UGameManager;
class UVoid;
class UBuddyController;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInteractionWith, UBuddyController*, Other);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSoundEmitted);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RESONATINGSPHERES_API UBuddyController : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBuddyController();

	UPROPERTY(BlueprintReadWrite)
	bool blocked = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<EVoidType> voidTypes;
	UPROPERTY(BlueprintReadWrite)
	TArray<UStaticMeshComponent*> voids;
	UPROPERTY(BlueprintReadWrite)
	TArray<UMaterialInstanceDynamic*> materials;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float jellyRadius;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float worldBounds;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float floatingMinSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float floatingSpeedVariation;

	/** After random walk, start chasing an object that is no further than this */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float chaseDistance;
	/** Stop that far from another buddy, before initializing interaction */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float interactionDistance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Easing)
		TEnumAsByte<EEaseType::EaseType> easeType = EEaseType::EaseType::linear;

	UGameManager* gameManager;

	UFUNCTION(BlueprintCallable)
		float GetLastTimeSoundEmited();

	UFUNCTION(BlueprintCallable)
		void RecalculateVoidsMaterial();

	UFUNCTION(BlueprintCallable)
		EVoidType GetClosestVoid(FVector loc, int& voidIndex);

	UFUNCTION(BlueprintCallable)
	UBuddyController* GetClosestBuddy();

	UFUNCTION(BlueprintCallable)
	UBuddyController* GetClosestBuddyInRange(float range);

	TArray<UBuddyController*> GetBuddiesInRange(float range);

	UPROPERTY(BlueprintAssignable)
		FSoundEmitted OnSoundEmitted;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FInteractionWith InteractGiveEnergyToBuddy;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FInteractionWith InteractDrainEnergyFromBuddy;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FInteractionWith InteractRecieveEnergyFromBuddy;

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
		FInteractionWith InteractLooseEnergyToBuddy;

	UFUNCTION( BlueprintCallable)
		void RecieveEnergyFromBuddy(UBuddyController* buddy);

	UFUNCTION(BlueprintCallable)
		void LooseEnergyToBuddy(UBuddyController* buddy);

	UFUNCTION(BlueprintCallable)
		void HearSoundFromBuddy(UBuddyController* buddy);

	UFUNCTION()
		void EmitSound();

	UFUNCTION(BlueprintCallable)
	bool GetIsControlledByPlayer();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;	

private:
	float lastTimeSoundEmited;
	bool isControlledByPlayer = false;
};
