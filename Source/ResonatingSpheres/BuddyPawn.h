// Ani a

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"
#include "iTInterface.h"
#include "BuddyPawn.generated.h"


/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMovementTweenEnded);

UCLASS()
class RESONATINGSPHERES_API ABuddyPawn : public ADefaultPawn, public IiTInterface
{
	GENERATED_BODY()
	
	virtual void SetupPlayerInputComponent(UInputComponent* InInputComponent) override;
	
	UPROPERTY(BlueprintAssignable)
		FMovementTweenEnded OnMovementTweenEnded;

public:
	virtual void OnTweenStartNative(AiTweenEvent* eventOperator, AActor* actorTweening = nullptr, USceneComponent* componentTweening = nullptr, UWidget* widgetTweening = nullptr, FName tweenName = "") override;

	virtual void OnTweenUpdateNative(AiTweenEvent* eventOperator, AActor* actorTweening, USceneComponent* componentTweening, UWidget* widgetTweening, FName tweenName, FDataTypeValues dataTypeValues, float alphaCompletion = 0.f) override;

	virtual void OnTweenLoopNative(AiTweenEvent* eventOperator, AActor* actorTweening = nullptr, USceneComponent* componentTweening = nullptr, UWidget* widgetTweening = nullptr, FName tweenName = "", int32 numberOfLoopSections = 0, ELoopType::LoopType loopType = once, bool playingBackward = false) override;

	virtual void OnTweenCompleteNative(AiTweenEvent* eventOperator, AActor* actorTweening, USceneComponent* componentTweening, UWidget* widgetTweening, FName tweenName, FHitResult sweepHitResultForMoveEvents, bool successfulTransform) override;

	UPROPERTY(BlueprintReadWrite)
	AActor* cameraOverride;

	/**
	* Input callback to move forward in local space (or backward if Val is negative).
	* @param Val Amount of movement in the forward direction (or backward if negative).
	* @see APawn::AddMovementInput()
	*/
	UFUNCTION(BlueprintCallable, Category = "Pawn")
		virtual void MoveForward(float Val) override;

	/**
	* Input callback to strafe right in local space (or left if Val is negative).
	* @param Val Amount of movement in the right direction (or left if negative).
	* @see APawn::AddMovementInput()
	*/
	UFUNCTION(BlueprintCallable, Category = "Pawn")
		virtual void MoveRight(float Val) override;
};
