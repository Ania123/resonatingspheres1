// Ani a

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

/**
 * 
 */
class UBuddyController;
UCLASS()
class RESONATINGSPHERES_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
	
	 
public:
	void BeginPlay() override;
	
	UFUNCTION(BlueprintCallable, Category = "Why here")
		void SwitchBuddy();

	UFUNCTION(BlueprintCallable, Category = "Why here")
		void EmitSound();
	
private:
	UBuddyController * currentBuddy;
	int buddyCounter = 0;
};
