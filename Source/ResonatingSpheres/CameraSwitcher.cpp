// Ani a

#include "CameraSwitcher.h"
#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "InputCoreTypes.h"
#include "Engine/World.h"
#include "Public/TimerManager.h"
#include "BuddyPawn.h"

// Sets default values
ACameraSwitcher::ACameraSwitcher(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	USceneComponent* SceneComponent = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("RootComp"));
	RootComponent = SceneComponent;
}

// Called when the game starts or when spawned
void ACameraSwitcher::BeginPlay()
{
	Super::BeginPlay();
	inputComponent = FindComponentByClass<UInputComponent>();
	playerController = GetWorld()->GetFirstPlayerController();
	playerCharacter = playerController->GetPawn();
	currentCamera = playerCharacter;
	if (inputComponent != nullptr) {
		inputComponent->BindKey(FKey("One"), EInputEvent::IE_Released, this, &ACameraSwitcher::KeyOnePressed);
		inputComponent->BindKey(FKey("Two"), EInputEvent::IE_Released, this, &ACameraSwitcher::KeyTwoPressed);
		inputComponent->BindKey(FKey("Three"), EInputEvent::IE_Released, this, &ACameraSwitcher::KeyThreePressed);
		inputComponent->BindKey(FKey("Four"), EInputEvent::IE_Released, this, &ACameraSwitcher::KeyFourPressed);
		inputComponent->BindKey(FKey("Five"), EInputEvent::IE_Released, this, &ACameraSwitcher::KeyFivePressed);
		inputComponent->BindKey(FKey("Six"), EInputEvent::IE_Released, this, &ACameraSwitcher::KeySixPressed);
		inputComponent->BindKey(FKey("Seven"), EInputEvent::IE_Released, this, &ACameraSwitcher::KeySevenPressed);
	}
}

void ACameraSwitcher::KeyOnePressed() {
	KeyPressed(0);
}

void ACameraSwitcher::KeyTwoPressed() {
	KeyPressed(1);
}

void ACameraSwitcher::KeyThreePressed() {
	KeyPressed(2);
}

void ACameraSwitcher::KeyFourPressed() {
	KeyPressed(3);
}

void ACameraSwitcher::KeyFivePressed() {
	KeyPressed(4);
}

void ACameraSwitcher::KeySixPressed() {
	KeyPressed(5);
}

void ACameraSwitcher::KeySevenPressed() {
	KeyPressed(6);
}

void ACameraSwitcher::KeyPressed(int i)
{
	if (blocked)
	{
		return;
	}
	playerCharacter = playerController->GetPawn();
	currentCamera = playerCharacter;
	AActor* nextCamera;
	if (i < cameras.Num()) {
		nextCamera = cameras[i];
	}
	else {
		nextCamera = playerCharacter;
	}
	blocked = true;
	if (nextCamera == playerCharacter || currentCamera == playerCharacter) {
		RemovePlayerInput();
	}
	if (nextCamera != playerCharacter) {
		Cast<ABuddyPawn>(playerCharacter)->cameraOverride = nextCamera;
	}
	else {
		Cast<ABuddyPawn>(playerCharacter)->cameraOverride = nullptr;
	}
	FTimerHandle handle;
	GetWorld()->GetTimerManager().SetTimer(handle, this, &ACameraSwitcher::RegainInputAfterBlend, blendTime, false, -1.0f);
	playerController->SetViewTargetWithBlend(nextCamera, blendTime, EViewTargetBlendFunction::VTBlend_Cubic, 2.0f, false);
	currentCamera = nextCamera;
}

void ACameraSwitcher::RemovePlayerInput()
{
	playerCharacter->DisableInput(playerController);
}

void ACameraSwitcher::RegainInputAfterBlend()
{
	blocked = false;
	playerCharacter -> EnableInput(playerController);
}

