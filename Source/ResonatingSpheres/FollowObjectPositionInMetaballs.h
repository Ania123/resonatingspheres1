// Ani a

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Metaballs.h"
#include "GameManager.h"
#include "FollowObjectPositionInMetaballs.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RESONATINGSPHERES_API UFollowObjectPositionInMetaballs : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UFollowObjectPositionInMetaballs();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere)
	FTransform offset;

	UPROPERTY(VisibleAnywhere)
	int metaballIndex;

	UPROPERTY(EditAnywhere)
	AActor *  player;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float radius;

	AActor * gameObject;
	
private:
	AMetaballs * metaballs;
	UGameManager * gameManager;
	float cycleOffset;
};
