// Ani a

#include "PlayerComponent.h"
#include "Components/InputComponent.h"
#include "BuddyController.h"

// Sets default values for this component's properties
UPlayerComponent::UPlayerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void UPlayerComponent::NotifyPresence(UBuddyController * objectNotifying)
{
	//A friend is calling. Will you help or hurt? :P
	int voidIndex = 0;
	auto voidType = buddy->GetClosestVoid(objectNotifying->GetOwner()->GetActorLocation(), voidIndex);
	switch (voidType)
	{
	case EVoidType::victim:
		break;
	case EVoidType::healer:
		buddy->InteractGiveEnergyToBuddy.Broadcast(objectNotifying);
		break;
	case EVoidType::attacker:
		buddy->InteractDrainEnergyFromBuddy.Broadcast(objectNotifying);
		break;
	case EVoidType::nothing:
		break;
	default:
		break;
	}
}

// Called when the game starts
void UPlayerComponent::BeginPlay()
{
	Super::BeginPlay();
	buddy = GetOwner()->FindComponentByClass<UBuddyController>();
}

void UPlayerComponent::EmitSound()
{
	if (buddy != nullptr) {
		buddy->EmitSound();
	}
}
// Called every frame
void UPlayerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

