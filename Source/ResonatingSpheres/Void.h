// Ani a

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/StaticMesh.h"
#include "Void.generated.h"

UENUM(BlueprintType)
	enum class EVoidType : uint8
	{
		victim UMETA(DisplayName = "victim - Looking for a healer void to help him drain the poison"),
		healer UMETA(DisplayName = "healer - looking for a matching void to heal"),
		attacker UMETA(DisplayName = "attacker"),
		nothing UMETA(DisplayName = "nothing, really")
	};


