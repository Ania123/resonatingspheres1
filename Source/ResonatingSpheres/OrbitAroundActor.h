// Ani a

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/World.h"
#include "GameManager.h"
#include "OrbitAroundActor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RESONATINGSPHERES_API UOrbitAroundActor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOrbitAroundActor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY(EditAnywhere)
	float InitialOrbitRadius;

	UPROPERTY(EditAnywhere)
	float TimeForCatastrophe;

	UPROPERTY(EditAnywhere)
	float TooCloseOrbitRadius;

	UPROPERTY(EditAnywhere)
	float CloserSpeed;

	UPROPERTY(EditAnywhere)
	float RotationSpeed;

	UPROPERTY(EditAnywhere)
	AActor * PersonToOrbitAround;
	
	FVector AxisVector;
	FVector CurrentOrbitRadius;
	float AngleAxis;

	float TooCloseTimeElapsed;

	AActor * gameObject;

	bool wasCatastrophe = false;

	UGameManager * gameManager;
};
