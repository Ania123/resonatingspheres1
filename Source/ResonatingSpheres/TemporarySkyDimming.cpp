// Ani a

#include "TemporarySkyDimming.h"


// Sets default values for this component's properties
UTemporarySkyDimming::UTemporarySkyDimming()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTemporarySkyDimming::BeginPlay()
{
	Super::BeginPlay();

	gameObject = GetOwner();
	myMesh = gameObject->FindComponentByClass<UStaticMeshComponent>();
	UMaterialInterface * existingMat = myMesh->GetMaterial(0);

	dynamicMat = UMaterialInstanceDynamic::Create(existingMat, this);
	myMesh->SetMaterial(0, dynamicMat);
	
	gameManager = GetWorld()->GetFirstPlayerController()->FindComponentByClass<UGameManager>();
}


// Called every frame
void UTemporarySkyDimming::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if(gameManager->wasCatastrophe) {
		if (dimValue < 100.0f) {
			dimValue += DeltaTime * dimSpeed;
			dynamicMat->SetScalarParameterValue(FName("Dimming"), dimValue);
		}
	}
	// ...
}

