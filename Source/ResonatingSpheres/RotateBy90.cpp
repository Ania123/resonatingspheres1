// Ani a

#include "RotateBy90.h"
#include "GameFramework/Actor.h"
#include "Math/TransformNonVectorized.h"
#include "Math/Rotator.h"

// Sets default values for this component's properties
URotateBy90::URotateBy90()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void URotateBy90::BeginPlay()
{
	Super::BeginPlay();
	gameObject = GetOwner();
	gameObject->SetActorRotation(FRotator(0, 180, 0));
	// ...

	UE_LOG(LogTemp, Warning, TEXT("should rot by 19"));
	
}


// Called every frame
void URotateBy90::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

