// Ani a

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/StaticMesh.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "GameManager.h"
#include "TemporarySkyDimming.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RESONATINGSPHERES_API UTemporarySkyDimming : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTemporarySkyDimming();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private :
	float dimValue = 0;

	UPROPERTY(EditAnywhere)
	float dimSpeed = 0.2f;

	UStaticMeshComponent * myMesh;

	AActor * gameObject;

	UMaterialInstanceDynamic * dynamicMat;
	
	UGameManager * gameManager;
};
