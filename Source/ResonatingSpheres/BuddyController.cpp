// Ani a

#include "BuddyController.h"
#include "GameManager.h"
#include <algorithm>
#include "PlayerComponent.h"
#include "FollowObjectPositionInMetaballs.h"
// Sets default values for this component's properties
UBuddyController::UBuddyController()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


void UBuddyController::RecieveEnergyFromBuddy(UBuddyController * buddy)
{
	InteractRecieveEnergyFromBuddy.Broadcast(buddy);
}

void UBuddyController::LooseEnergyToBuddy(UBuddyController * buddy)
{
	UE_LOG(LogTemp, Warning, TEXT("Loosing energy from %s"), *(GetOwner()->GetName()));
	InteractLooseEnergyToBuddy.Broadcast(buddy);
}

void UBuddyController::HearSoundFromBuddy(UBuddyController * buddy)
{

}

void UBuddyController::EmitSound()
{
	UE_LOG(LogTemp, Warning, TEXT("Emitting sound from %s"), *(GetOwner()->GetName()));
	lastTimeSoundEmited = GetWorld()->TimeSeconds;
	//the speaking radius was here
	auto interestedBuddies = GetBuddiesInRange(100);
	for (auto buddy : interestedBuddies)
	{
		buddy->HearSoundFromBuddy(this);
	}
	OnSoundEmitted.Broadcast();
}

bool UBuddyController::GetIsControlledByPlayer()
{
	return isControlledByPlayer;
}

// Called when the game starts
void UBuddyController::BeginPlay()
{
	Super::BeginPlay();

	auto followMetaballs = GetOwner()->FindComponentByClass<UFollowObjectPositionInMetaballs>();
	gameManager = GetWorld()->GetFirstPlayerController()->FindComponentByClass<UGameManager>();
	followMetaballs->metaballIndex = gameManager->buddies.Num();
	gameManager->buddies.Add(this);

	isControlledByPlayer = (GetOwner()->FindComponentByClass<UInputComponent>()!=nullptr);

	RecalculateVoidsMaterial();
}

void UBuddyController::RecalculateVoidsMaterial() 
{
	for (int i = 0; i < std::min(materials.Num(), voidTypes.Num()); i++) {
		float victimizationCoefficient = 0;
		switch (voidTypes[i])
		{
		case EVoidType::attacker:
			victimizationCoefficient = 1;
			break;
		case EVoidType::healer:
			victimizationCoefficient = 0;
			break;
		case EVoidType::victim:
			victimizationCoefficient = 0.5f;
			break;
		default:
			break;
		}
		materials[i]->SetScalarParameterValue(FName("VictimizationCoefficient"), victimizationCoefficient);
	}
}
EVoidType UBuddyController::GetClosestVoid(FVector loc, int& voidIndex)
{
	
	float minFound = MAX_FLT;
	EVoidType result = EVoidType::victim;
	for (int i = 0; i < std::min(voids.Num(), voidTypes.Num()); i++) {
		float dist = FVector::Dist(voids[i]->GetComponentTransform().GetLocation(), loc);
		if (dist < minFound) {
			result = voidTypes[i];
			minFound = dist;
			voidIndex = i;
		}
	}
	return result;
}
// Called every frame
void UBuddyController::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//check if we should die
	if (jellyRadius < 0.3f) {
		gameManager->DoCatastrophe();
	}
}

float UBuddyController::GetLastTimeSoundEmited()
{
	return lastTimeSoundEmited;
}

UBuddyController * UBuddyController::GetClosestBuddy()
{
	float minFound = MAX_FLT;
	UBuddyController * minBuddy = nullptr;
	FVector loc = GetOwner()->GetTransform().GetLocation();
	for  (auto buddy : gameManager->buddies)
	{
		if (buddy != this) 
		{
			if (FVector::Distance(buddy->GetOwner()->GetTransform().GetLocation(), loc) < minFound) 
			{
				minFound = FVector::Distance(buddy->GetOwner()->GetTransform().GetLocation(), loc);
				minBuddy = buddy;
			}
		}
	}
	return minBuddy;
}

UBuddyController * UBuddyController::GetClosestBuddyInRange(float range)
{
	float minFound = MAX_FLT;
	UBuddyController * minBuddy = nullptr;
	FVector loc = GetOwner()->GetTransform().GetLocation();
	for (auto buddy : gameManager->buddies)
	{
		if (buddy != this)
		{
			if (FVector::Distance(buddy->GetOwner()->GetTransform().GetLocation(), loc) < minFound)
			{
				minFound = FVector::Distance(buddy->GetOwner()->GetTransform().GetLocation(), loc);
				if (minFound < range) {
					minBuddy = buddy;
				}
			}
		}
	}
	return minBuddy;
}

TArray<UBuddyController*> UBuddyController::GetBuddiesInRange(float range)
{
	auto result = TArray<UBuddyController*>();
	auto loc = GetOwner()->GetTransform().GetLocation();
	for (auto buddy : gameManager->buddies)
	{
		if (buddy != this) 
		{
			if (FVector::Distance(buddy->GetOwner()->GetTransform().GetLocation(), loc) < range) 
			{
				result.Add(buddy);
			}
		}
	}
	return result;
}

